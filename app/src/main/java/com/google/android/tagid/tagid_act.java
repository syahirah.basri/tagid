package com.google.android.tagid;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.bluetooth.BluetoothAdapter;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
import java.util.Locale;


public class tagid_act extends AppCompatActivity {
    private static final int REQUEST_ENABLE_BT = 0;
    private Button mDoneBt, mOnBtn, mOffBtn, mOthApk;
    private TextView mConnStat;
    BluetoothAdapter mBlueAdapter;
    private WindowManager windowManager;
    private ImageView chatHead;
    WindowManager.LayoutParams params;

    //@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP);

    //@TargetApi(Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //apk to apk activity
        /*Intent i = getIntent();
        Bundle b = i.getBundleExtra("userBundle");
        TextView displayMsg = (TextView)findViewById(R.id.displayMsg);
        String msg = "GET " + b.getString("name") + " ";
        msg = msg + "Return Data From SAMinit .apk";
        displayMsg.setText(msg);*/

        //adapter
        mBlueAdapter = BluetoothAdapter.getDefaultAdapter();
        //on bt btn click
        mOnBtn = (Button)findViewById(R.id.bt_config_on);
        //off bt btn click
        mOffBtn = (Button)findViewById(R.id.bt_config_off);
        mDoneBt = (Button)findViewById(R.id.bt_done);
        mConnStat = (TextView)findViewById(R.id.connStat);
        mOthApk = (Button) findViewById(R.id.oth_apk);
        final RelativeLayout parent = (RelativeLayout) findViewById(R.id.parent);

        mOnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mBlueAdapter.isEnabled()){
                    showToast("Turning On Bluetooth...");
                    mConnStat.setText("  Connected");
                    mConnStat.setTextColor(Color.BLACK);
                    //intent to on bluetooth
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(intent, REQUEST_ENABLE_BT);
                }
                else {
                    mConnStat.setText("  Connected");
                    mConnStat.setTextColor(Color.BLACK);
                    showToast("Bluetooth is already on");
                }
            }
        });

        mOffBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if (mBlueAdapter.isEnabled()){
                    mBlueAdapter.disable();
                    mConnStat.setText("  Disconnected");
                    mConnStat.setTextColor(Color.RED);
                    showToast("Turning Bluetooth Off");
                }
                else {
                    mConnStat.setText("  Disconnected");
                    mConnStat.setTextColor(Color.RED);
                    showToast("Bluetooth is already off");
                }
            }
        });

        mDoneBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
                System.exit(0);
            }
        });

        mOthApk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.google.android.saminit.SECOND.class");
                //Intent launchIntent = new Intent(tagid_act.this,com.google.android.saminit.SECOND.class);
                if (launchIntent != null){
                    startActivity(launchIntent);
                } else {
                    Toast.makeText(tagid_act.this, "The package is not found.", Toast.LENGTH_LONG).show();
                }
            }
        });

        /*mOthApk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.google.android.saminit");
                if (launchIntent != null) {
                    try {
                        startActivity(launchIntent);
                    } catch (ActivityNotFoundException err) {
                        Toast t = Toast.makeText(getApplicationContext(),
                                "App is not found", Toast.LENGTH_SHORT);
                        t.show();
                    }
                }
            }
        });*/
    }


    //toast message function
    private void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}